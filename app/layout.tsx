import type { Metadata } from 'next'
import { Josefin_Sans, Poppins } from 'next/font/google'
import { SessionProvider } from 'next-auth/react'
import { auth } from '@/auth'
import './globals.css'
import { Toaster } from '@/components/ui/sonner'

import { ThemeProvider } from '@/components/theme-provider'
import { Navbar } from '@/components/navbar'

const poppins = Poppins({
  subsets: ['latin'],
  weight: ['400', '500', '600', '700', '800'],
  variable: '--font-poppins'
})

const josefin = Josefin_Sans({
  subsets: ['latin'],
  weight: ['400', '500', '600', '700'],
  variable: '--font-josefin'
})

export const metadata: Metadata = {
  title: 'Tutorial Vídeos',
  description: 'Tutorial de Vídeos de Next.js',
}

export default async function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  const  session  = await auth()
  return (
      <SessionProvider session={session}>
      <html lang="en">
        <body className={`${poppins.variable} ${josefin.variable}`}>
          <ThemeProvider
            attribute='class'
            defaultTheme='system'
            enableSystem
            >
            <Toaster/>
            <Navbar />
            {children}
          </ThemeProvider>
        </body>
      </html>
    </SessionProvider>
  )
}
