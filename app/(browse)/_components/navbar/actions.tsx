"use client"
import { LoginButton } from "@/components/auth/login-botton"
import { UserButton } from "@/components/auth/user-button"
import { Button } from "@/components/ui/button"
import { useCurrentUser } from "@/hooks/use-current-user"

export const Actions = () => {
    const user = useCurrentUser()
    return (
        <div className="flex items-center justify-end gap-x-2 ml-4 lg:ml-0">
            {!user && (
                <LoginButton mode="modal" asChild>
                    <Button variant="secondary" size="lg">
                        Login
                    </Button>
              </LoginButton>
            )}
            {!!user && (
                <div>
                    <UserButton />
                </div>
            )}
        </div>
    )
}