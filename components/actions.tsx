"use client"
import { LoginButton } from "@/components/auth/login-botton"
import { UserButton } from "@/components/auth/user-button"
import { Button } from "@/components/ui/button"
import { useCurrentUser } from "@/hooks/use-current-user"

export const Actions = () => {
    const user = useCurrentUser()
    return (
        <div className="flex items-center justify-end gap-x-2 ml-4 lg:ml-0">
            {!user && (
                <LoginButton asChild mode="modal">
                    <Button variant="primary" size="lg">
                        Login
                    </Button>
              </LoginButton>
            )}
            {!!user && (
                <div className="flex items-center gap-x-4">
                    <UserButton />
                </div>
            )}
        </div>
    )
}