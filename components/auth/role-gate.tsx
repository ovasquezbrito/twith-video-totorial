"use client"

import { useCurrentRole } from "@/hooks/use-current-role"
import { UserRole } from "@prisma/client"
import { FormError } from "@/components/form-error"

interface RoleGateProps {
    children: React.ReactNode
    allowedrole: UserRole
}

export const RoleGate = ({ children, allowedrole }: RoleGateProps) => {
    const role = useCurrentRole()
    if (role !== allowedrole) {
        return (
            <FormError message="You do not have permission to access this page" />
        )
    }
    return <>{children}</>
}