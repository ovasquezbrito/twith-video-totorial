import { ExclamationTriangleIcon } from "@radix-ui/react-icons"

interface FormErrorProps {
  message?: string
}

export const FormError = ({ message }: FormErrorProps) => {
  if (!message) return null
  return (
    <p className="bg-destructive/15 p-3 rounded-sm flex items-center gap-x-2 text-sm text-destructive text-white">
      <ExclamationTriangleIcon className="h-4 w-4"/>
      <span>{message}</span>
    </p>
  )
}