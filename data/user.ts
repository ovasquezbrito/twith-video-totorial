import { db } from "@/lib/db"

export const getUserByEmail = async (email: string) => {
    try {
        const user = await db.user.findUnique({
            where: {
                email
            }
        })
        return user
    } catch  {
        return null
    }
}

export const getUserById = async (id: string) => {
    try {
        const user = await db.user.findUnique({
            where: {
                id
            }
        })
        return user
    } catch  {
        return null
    }
}

export const getRecommendedUsers = async () => {
    try {
        const users = await db.user.findMany({
            orderBy: {
                createdAt: "desc"
            }
        })
        return users
    } catch  {
        return null
    }
}

export const getRecommendedNotActiveUsers = async (id: string) => {
    try {
        const users = await db.user.findMany({
            where: {
                NOT: {
                    id
                }
            },
            orderBy: {
                createdAt: "desc"
            }
        })
        return users
    } catch  {
        return null
    }
}