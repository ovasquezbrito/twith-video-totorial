import { UserSigre } from '@/lib/definiciones'
import conn  from '@/lib/postgres'

export const getUserBySiga = async () => {
    try {
        const data = await conn.query('SELECT id, username, email, picture FROM "USER_SACP".users')
        await conn.end()
        return data.rows as UserSigre[];
    } catch (err) {
        console.error('Database Error:', err);
        throw new Error('Failed to fetch all customers.');
    }
}