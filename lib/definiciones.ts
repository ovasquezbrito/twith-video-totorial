export type UserSigre = {
    id: number | null;
    username: string | null;
    email: string | null;
    image: string | null;
  };
  