import { getUserById } from "@/data/user";
import { currentUser } from "./auth";


export const getSelf = async () => {
    const self = await currentUser()

    if (!self || !self.name) {
        throw new Error("User not found")
        
    }

    const user = getUserById(self.id as string)

    if (!user) {
        throw new Error("User not found")
    }

    return user
}

