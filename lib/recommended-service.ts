import { getRecommendedNotActiveUsers } from "@/data/user"
import { currentUser } from "./auth"


export const getRecommended = async () => {
    const self = await currentUser()

    if (!self || !self.name) {
        throw new Error("User not found")
        
    }
    const users = await getRecommendedNotActiveUsers(self.id as string)

    return users
    
}