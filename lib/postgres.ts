import { Pool } from "pg"

let conn: any

if (!conn) {
    conn = new Pool({
        user: process.env.PGUSER,
        host: process.env.PGHOST,
        database: process.env.PGDATABASE,
        password: process.env.PGPASSWORD,
        port: 5432,
      })
}


export default conn