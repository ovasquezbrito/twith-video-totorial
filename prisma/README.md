
## Prisma Started

First, run the development server:

```bash
npx prisma generate
npx prisma db push
# view data
npx prisma studio
