"use server"

import * as z from "zod"
import bcrypt from "bcryptjs"

import { NewPasswordSchema } from "@/schemas"
import { getPasswordResetTokenByToken } from "@/data/password-reset-token"
import { getUserByEmail } from "@/data/user"
import { db } from "@/lib/db"

export const newPassword = async (
    values: z.infer<typeof NewPasswordSchema>,
    token?: string | null,
) => {
    if (!token) {
        return {
            error: "01- Token inválido",
        }
    }

    const validateFields = NewPasswordSchema.safeParse(values)

    if (!validateFields.success) {
        return {
            error: "Contraseña inválida",
        }
    }

    console.log(token)
    const { password } = validateFields.data

    const passwordResetToken = await getPasswordResetTokenByToken(token)

    if (!passwordResetToken) {
        return {
            error: "02 Token inválido",
        }
    }

    const hasExpired = new Date(passwordResetToken.expires) < new Date()

    if (hasExpired) {
        return {
            error: "Token expirado",
        }
    }

    const existingUser = await getUserByEmail(passwordResetToken.email)

    if (!existingUser) {
        return {
            error: "Email no existe",
        }
    }

    const hashedPassword = await bcrypt.hash(password, 10)

    await db.user.update({
        where: { id: existingUser.id },
        data: { password: hashedPassword }
    })

    await db.passwordResetToken.delete({
        where: { id: passwordResetToken.id }
    })

    return { success: "Contraseña cambiada" }
}
