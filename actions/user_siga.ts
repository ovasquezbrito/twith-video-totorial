"use server"

import { getUserBySiga } from "@/data/useer_siga"

export const getUserBySigaRes = async () => {
    try {
        const user = await getUserBySiga()
        return user
    } catch  {
        return null
    }
}