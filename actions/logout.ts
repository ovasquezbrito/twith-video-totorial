"use server"

import { signOut } from "@/auth"

export const logout = async () => {
    // algunas cosas del servidor
    await signOut()
}