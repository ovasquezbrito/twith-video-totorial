"use server"

import * as z from "zod"
import bcrypt from "bcryptjs"

import { unstable_update } from "@/auth"
import { db } from "@/lib/db"
import { SettingsSchema } from "@/schemas"
import { getUserByEmail, getUserById } from "@/data/user"
import { currentUser } from "@/lib/auth"
import { generateVerificationToken } from "@/lib/tokens"
import { sendVerificationEmail } from "@/lib/mail"

export const settings = async (data: z.infer<typeof SettingsSchema>) => {
    const user = await currentUser()

    if (!user) {
        return {
            error: "Unauthorized",
        }
    }

    const dbUser = await getUserById(user.id as string)

    if (!dbUser) {
        return {
            error: "User not found",
        }
    }

    if (user.isOAuth) {
        data.email = undefined
        data.password = undefined
        data.newPassword = undefined
        data.isTwoFactorEnabled = undefined
    }

    if (data.email && data.email !== user.email) {
        const existingUser = await getUserByEmail(data.email)
        if (existingUser && existingUser.id !== user.id) {
            return {
                error: "Email already in use",
            }
        }

        const verificationToken = await generateVerificationToken(data.email)

        await sendVerificationEmail(
            verificationToken.email,
            verificationToken.token
        )

        return {
            success: "Verification email sent",
        }
    }

    if (data.password && data.newPassword && dbUser.password) {
        const passwordsMatch = await bcrypt.compare(
            data.password,
            dbUser.password
        )

        if (!passwordsMatch) {
            return {
                error: "Incorrect password",
            }
        }

        const hashedPassword = await bcrypt.hash(data.newPassword, 10)
        data.password = hashedPassword
        data.newPassword = undefined
    }

    const updatedUser = await db.user.update({
        where: {
            id: dbUser.id
        },
        data: {
            ...data
        }
    })

    unstable_update({
        user: {
            name: updatedUser.name,
            email: updatedUser.email,
            isTwoFactorEnabled: updatedUser.isTwoFactorEnabled,
            role: updatedUser.role,
            
        }
    })

    return {
        success: "Settings updated",
    }
}